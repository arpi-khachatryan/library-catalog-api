# Library Catalog REST API


## Introduction

This project is a Spring-based REST API for managing a library catalog. It allows you to perform various operations related to books, authors, and users.

## Swagger API Documentation. 

Explore and test the API using Swagger UI: 
- [Swagger API Documentation](http://localhost:8080/api/v1/swagger-ui/index.html) 
Please follow the provided link to access the Swagger UI interface.

## Prerequisites

Before running this project, ensure you have met the following requirements:
- **Java**: You need to have Java Development Kit (JDK) 17 or a higher version installed.
- **MySQL**: Install and configure MySQL as your database server.
- **Other Dependencies**: This project uses Spring Boot, Swagger, Lombok, ModelMapper, MapStruct. 

## Entities

- Book: Represents a book in the catalog.
- Author: Represents an author of one or more books.
- User: Represents a user who can borrow and return books.


## Operations

- Book Operations: Create, read, update, and delete books. Borrow and return books.
- Author Operations: Create, read, update, and delete authors.
- User Operations: Create, read, update, and delete user accounts.


## REST API Design

- /api/v1/books: Manage books in the catalog.
- /api/v1/authors: Manage authors.
- /api/v1/users: Manage user accounts.


## Status code

- 200 OK: Successful response.
- 404: USER_NOT_FOUND
- 400: USER_REGISTRATION_FAILED
- 404: AUTHOR_NOT_FOUND
- 400: AUTHOR_ALREADY_EXISTS
- 404: BOOK_NOT_FOUND
- 400: BOOK_ALREADY_EXISTS 


## Exception Handling

Comprehensive exception handling has been implemented to gracefully handle unexpected scenarios within the application.


## Testing

JUnit tests have been implemented to ensure the functionality and reliability of the API. 


