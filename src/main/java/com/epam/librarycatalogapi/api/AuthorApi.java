package com.epam.librarycatalogapi.api;

import com.epam.librarycatalogapi.dto.AuthorDTO;
import com.epam.librarycatalogapi.dto.UserDTO;
import com.epam.librarycatalogapi.exception.ApiError;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.ResponseEntity;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

/**
 * @author Arpi Khachatryan on 30.09.2023
 */
public interface AuthorApi {

    @Operation(
            summary = "Retrieve all authors",
            description = "Retrieves a list of authors from the database, with optional pagination and sorting parameters.")
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Successfully retrieved authors from the database.",
                            content = @Content(
                                    schema = @Schema(implementation = UserDTO.class),
                                    mediaType = APPLICATION_JSON_VALUE)),
                    @ApiResponse(
                            responseCode = "404",
                            description = "No authors found matching the criteria.",
                            content = @Content(
                                    mediaType = APPLICATION_JSON_VALUE,
                                    schema = @Schema(implementation = ApiError.class)))})
    ResponseEntity<List<AuthorDTO>> getAllAuthors(int pageNo, int pageSize, String sortBy, String sortDir);

    @Operation(
            summary = "Retrieve an author by ID",
            description = "Retrieves an author from the database by their unique identifier.")
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Successfully retrieved the author from the database.",
                            content = @Content(
                                    schema = @Schema(implementation = UserDTO.class),
                                    mediaType = APPLICATION_JSON_VALUE)),
                    @ApiResponse(
                            responseCode = "404",
                            description = "No author found with the specified ID.",
                            content = @Content(
                                    schema = @Schema(implementation = ApiError.class),
                                    mediaType = APPLICATION_JSON_VALUE))})
    ResponseEntity<AuthorDTO> getAuthorById(Long id);

    @Operation(
            summary = "Create a new author",
            description = "Creates a new author in the system.")
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Author creation successful.",
                            content =
                            @Content(
                                    mediaType = APPLICATION_JSON_VALUE)),
                    @ApiResponse(
                            responseCode = "400",
                            description = "An author with the same information already exists.",
                            content = @Content(
                                    mediaType = APPLICATION_JSON_VALUE,
                                    schema = @Schema(implementation = ApiError.class)))})
    ResponseEntity<?> createAuthor(AuthorDTO authorDto);

    @Operation(
            summary = "Update author information",
            description = "Updates an existing author's information in the database.")
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Author information updated successfully.",
                            content = @Content(
                                    schema = @Schema(implementation = UserDTO.class),
                                    mediaType = APPLICATION_JSON_VALUE)),
                    @ApiResponse(
                            responseCode = "404",
                            description = "No author found with the specified ID.",
                            content = @Content(
                                    schema = @Schema(implementation = ApiError.class),
                                    mediaType = APPLICATION_JSON_VALUE))})
    ResponseEntity<AuthorDTO> updateAuthor(Long id, AuthorDTO authorDto);

    @Operation(
            summary = "Delete an author",
            description = "Deletes an existing author from the database.")
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Author deleted successfully."),
                    @ApiResponse(
                            responseCode = "404",
                            description = "No author found with the specified ID.",
                            content = @Content(
                                    schema = @Schema(implementation = ApiError.class),
                                    mediaType = APPLICATION_JSON_VALUE))})
    ResponseEntity<?> deleteAuthor(Long id);
}
