package com.epam.librarycatalogapi.api;

import com.epam.librarycatalogapi.dto.AuthorDTO;
import com.epam.librarycatalogapi.dto.BookDTO;
import com.epam.librarycatalogapi.dto.UserDTO;
import com.epam.librarycatalogapi.exception.ApiError;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.ResponseEntity;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

/**
 * @author Arpi Khachatryan on 30.09.2023
 */
public interface BookApi {

    @Operation(
            summary = "Retrieve all books",
            description = "Retrieves a list of books from the database, with optional pagination and sorting parameters.")
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Successfully retrieved books from the database.",
                            content = @Content(
                                    schema = @Schema(implementation = UserDTO.class),
                                    mediaType = APPLICATION_JSON_VALUE)),
                    @ApiResponse(
                            responseCode = "404",
                            description = "No books found matching the criteria.",
                            content = @Content(
                                    mediaType = APPLICATION_JSON_VALUE,
                                    schema = @Schema(implementation = ApiError.class)))})
    ResponseEntity<List<BookDTO>> getAllBooks(int pageNo, int pageSize, String sortBy, String sortDir);

    @Operation(
            summary = "Retrieve books by genre",
            description = "Retrieves a list of books from the database by genre, with optional pagination and sorting parameters.")
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Successfully retrieved books from the database by genre.",
                            content = @Content(
                                    schema = @Schema(implementation = UserDTO.class),
                                    mediaType = APPLICATION_JSON_VALUE)),
                    @ApiResponse(
                            responseCode = "404",
                            description = "No books found matching the criteria.",
                            content = @Content(
                                    mediaType = APPLICATION_JSON_VALUE,
                                    schema = @Schema(implementation = ApiError.class)))})
    ResponseEntity<List<BookDTO>> getBooksByGenre(String genre, int pageNo, int pageSize, String sortBy, String sortDir);

    @Operation(
            summary = "Retrieve books by author",
            description = "Retrieves a list of books from the database by author, with optional pagination and sorting parameters.")
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Successfully retrieved books from the database by author.",
                            content = @Content(
                                    schema = @Schema(implementation = UserDTO.class),
                                    mediaType = APPLICATION_JSON_VALUE)),
                    @ApiResponse(
                            responseCode = "404",
                            description = "No books found matching the criteria.",
                            content = @Content(
                                    mediaType = APPLICATION_JSON_VALUE,
                                    schema = @Schema(implementation = ApiError.class)))})
    ResponseEntity<List<BookDTO>> getBooksByAuthor(AuthorDTO authorDto, int pageNo, int pageSize, String sortBy, String sortDir);


    @Operation(
            summary = "Retrieve a book by ID",
            description = "Retrieves a book from the database by its unique identifier.")
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Successfully retrieved the book from the database.",
                            content = @Content(
                                    schema = @Schema(implementation = UserDTO.class),
                                    mediaType = APPLICATION_JSON_VALUE)),
                    @ApiResponse(
                            responseCode = "404",
                            description = "No book found with the specified ID.",
                            content = @Content(
                                    schema = @Schema(implementation = ApiError.class),
                                    mediaType = APPLICATION_JSON_VALUE))})
    ResponseEntity<BookDTO> getBookById(Long id);

    @Operation(
            summary = "Create a new book",
            description = "Creates a new book in the system.")
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Book creation successful.",
                            content =
                            @Content(
                                    mediaType = APPLICATION_JSON_VALUE)),
                    @ApiResponse(
                            responseCode = "400",
                            description = "A book with the same information already exists.",
                            content = @Content(
                                    mediaType = APPLICATION_JSON_VALUE,
                                    schema = @Schema(implementation = ApiError.class)))})
    ResponseEntity<?> addBook(BookDTO bookDto);

    @Operation(
            summary = "Update book information",
            description = "Updates an existing book's information in the database.")
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Book information updated successfully.",
                            content = @Content(
                                    schema = @Schema(implementation = UserDTO.class),
                                    mediaType = APPLICATION_JSON_VALUE)),
                    @ApiResponse(
                            responseCode = "404",
                            description = "No book found with the specified ID.",
                            content = @Content(
                                    schema = @Schema(implementation = ApiError.class),
                                    mediaType = APPLICATION_JSON_VALUE))})
    ResponseEntity<BookDTO> editBook(Long id, BookDTO bookDto);

    @Operation(
            summary = "Delete a book",
            description = "Deletes an existing book from the database..")
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Book deleted successfully."),
                    @ApiResponse(
                            responseCode = "404",
                            description = "No book found with the specified ID.",
                            content = @Content(
                                    schema = @Schema(implementation = ApiError.class),
                                    mediaType = APPLICATION_JSON_VALUE))})
    ResponseEntity<?> deleteBook(Long id);
}
