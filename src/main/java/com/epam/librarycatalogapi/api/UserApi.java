package com.epam.librarycatalogapi.api;

import com.epam.librarycatalogapi.dto.UserDTO;
import com.epam.librarycatalogapi.exception.ApiError;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.ResponseEntity;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

/**
 * @author Arpi Khachatryan on 30.09.2023
 */
public interface UserApi {

    @Operation(
            summary = "Retrieve all users",
            description = "Retrieves a list of users from the database, with optional pagination and sorting parameters.")
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Successfully retrieved users from the database.",
                            content = @Content(
                                    schema = @Schema(implementation = UserDTO.class),
                                    mediaType = APPLICATION_JSON_VALUE)),
                    @ApiResponse(
                            responseCode = "404",
                            description = "No users found matching the criteria.",
                            content = @Content(
                                    mediaType = APPLICATION_JSON_VALUE,
                                    schema = @Schema(implementation = ApiError.class)))})
    ResponseEntity<List<UserDTO>> getAllUsers(int pageNo, int pageSize, String sortBy, String sortDir);

    @Operation(
            summary = "Retrieve a user by ID",
            description = "Retrieves a user from the database by their unique identifier.")
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Successfully retrieved the user from the database.",
                            content = @Content(
                                    schema = @Schema(implementation = UserDTO.class),
                                    mediaType = APPLICATION_JSON_VALUE)),
                    @ApiResponse(
                            responseCode = "404",
                            description = "No user found with the specified ID.",
                            content = @Content(
                                    schema = @Schema(implementation = ApiError.class),
                                    mediaType = APPLICATION_JSON_VALUE))})
    ResponseEntity<UserDTO> getUserById(Long id);

    @Operation(
            summary = "Register new user",
            description = "Registers a new user in the system.")
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "User registration successful.",
                            content =
                            @Content(
                                    mediaType = APPLICATION_JSON_VALUE)),
                    @ApiResponse(
                            responseCode = "400",
                            description = "A user with the same email address already exists.",
                            content = @Content(
                                    mediaType = APPLICATION_JSON_VALUE,
                                    schema = @Schema(implementation = ApiError.class)))})
    ResponseEntity<?> createUser(UserDTO userDto);

    @Operation(
            summary = "Update user information",
            description = "Updates an existing user's information in the database.")
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "User information updated successfully.",
                            content = @Content(
                                    schema = @Schema(implementation = UserDTO.class),
                                    mediaType = APPLICATION_JSON_VALUE)),
                    @ApiResponse(
                            responseCode = "404",
                            description = "No user found with the specified ID.",
                            content = @Content(
                                    schema = @Schema(implementation = ApiError.class),
                                    mediaType = APPLICATION_JSON_VALUE))})
    ResponseEntity<UserDTO> updateUser(Long id, UserDTO userDto);

    @Operation(
            summary = "Delete a user",
            description = "Deletes an existing user from the database.")
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "User deleted successfully."),
                    @ApiResponse(
                            responseCode = "404",
                            description = "No user found with the specified ID.",
                            content = @Content(
                                    schema = @Schema(implementation = ApiError.class),
                                    mediaType = APPLICATION_JSON_VALUE))})
    ResponseEntity<?> deleteUser(Long id);
}
