package com.epam.librarycatalogapi.api.controller;

import com.epam.librarycatalogapi.api.AuthorApi;
import com.epam.librarycatalogapi.dto.AuthorDTO;
import com.epam.librarycatalogapi.service.AuthorService;
import com.epam.librarycatalogapi.util.AppConstants;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * @author Arpi Khachatryan on 30.09.2023
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/authors")
public class AuthorController implements AuthorApi {

    private final AuthorService authorService;

    @Override
    @GetMapping("/all")
    public ResponseEntity<List<AuthorDTO>> getAllAuthors(@RequestParam(value = "pageNo", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER, required = false) int pageNo,
                                                         @RequestParam(value = "pageSize", defaultValue = AppConstants.DEFAULT_PAGE_SIZE, required = false) int pageSize,
                                                         @RequestParam(value = "sortBy", defaultValue = AppConstants.DEFAULT_SORT_BY, required = false) String sortBy,
                                                         @RequestParam(value = "sortDir", defaultValue = AppConstants.DEFAULT_SORT_DIRECTION, required = false) String sortDir) {

        return ResponseEntity.ok(authorService.getAllAuthors(pageNo, pageSize, sortBy, sortDir));
    }

    @Override
    @GetMapping("/{id}")
    public ResponseEntity<AuthorDTO> getAuthorById(@PathVariable("id") Long id) {
        return ResponseEntity.ok(authorService.getAuthorById(id));
    }

    @Override
    @PostMapping()
    public ResponseEntity<?> createAuthor(@Valid @RequestBody AuthorDTO authorDto) {
        return ResponseEntity.ok(authorService.createAuthor(authorDto));
    }

    @Override
    @PutMapping("/{id}")
    public ResponseEntity<AuthorDTO> updateAuthor(@PathVariable("id") Long id, @Valid @RequestBody AuthorDTO updatedAuthorDto) {
        return ResponseEntity.ok(authorService.updateAuthor(id, updatedAuthorDto));
    }

    @Override
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteAuthor(@PathVariable("id") Long id) {
        authorService.deleteAuthor(id);
        return ResponseEntity.ok().build();
    }
}
