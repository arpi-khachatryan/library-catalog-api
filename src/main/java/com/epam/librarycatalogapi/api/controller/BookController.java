package com.epam.librarycatalogapi.api.controller;

import com.epam.librarycatalogapi.api.BookApi;
import com.epam.librarycatalogapi.dto.AuthorDTO;
import com.epam.librarycatalogapi.dto.BookDTO;
import com.epam.librarycatalogapi.service.BookService;
import com.epam.librarycatalogapi.util.AppConstants;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * @author Arpi Khachatryan on 30.09.2023
 */
@RestController
@RequestMapping("/books")
@RequiredArgsConstructor
public class BookController implements BookApi {

    private final BookService bookService;

    @Override
    @GetMapping("/all")
    public ResponseEntity<List<BookDTO>> getAllBooks(@RequestParam(value = "pageNo", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER, required = false) int pageNo,
                                                     @RequestParam(value = "pageSize", defaultValue = AppConstants.DEFAULT_PAGE_SIZE, required = false) int pageSize,
                                                     @RequestParam(value = "sortBy", defaultValue = AppConstants.DEFAULT_SORT_BY, required = false) String sortBy,
                                                     @RequestParam(value = "sortDir", defaultValue = AppConstants.DEFAULT_SORT_DIRECTION, required = false) String sortDir) {
        return ResponseEntity.ok(bookService.getAllBooks(pageNo, pageSize, sortBy, sortDir));
    }

    @Override
    @GetMapping("/byGenre/{genre}")
    public ResponseEntity<List<BookDTO>> getBooksByGenre(@PathVariable("genre") String genre,
                                                         @RequestParam(value = "pageNo", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER, required = false) int pageNo,
                                                         @RequestParam(value = "pageSize", defaultValue = AppConstants.DEFAULT_PAGE_SIZE, required = false) int pageSize,
                                                         @RequestParam(value = "sortBy", defaultValue = AppConstants.DEFAULT_SORT_BY, required = false) String sortBy,
                                                         @RequestParam(value = "sortDir", defaultValue = AppConstants.DEFAULT_SORT_DIRECTION, required = false) String sortDir) {
        return ResponseEntity.ok(bookService.getBooksByGenre(genre, pageNo, pageSize, sortBy, sortDir));
    }


    @Override
    @GetMapping("/byAuthor")
    public ResponseEntity<List<BookDTO>> getBooksByAuthor(@RequestBody AuthorDTO authorDto,
                                                          @RequestParam(value = "pageNo", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER, required = false) int pageNo,
                                                          @RequestParam(value = "pageSize", defaultValue = AppConstants.DEFAULT_PAGE_SIZE, required = false) int pageSize,
                                                          @RequestParam(value = "sortBy", defaultValue = AppConstants.DEFAULT_SORT_BY, required = false) String sortBy,
                                                          @RequestParam(value = "sortDir", defaultValue = AppConstants.DEFAULT_SORT_DIRECTION, required = false) String sortDir) {
        return ResponseEntity.ok(bookService.getBooksByAuthor(authorDto, pageNo, pageSize, sortBy, sortDir));
    }

    @Override
    @GetMapping("/{id}")
    public ResponseEntity<BookDTO> getBookById(@PathVariable("id") Long id) {
        return ResponseEntity.ok(bookService.getBookById(id));
    }

    @Override
    @PostMapping
    public ResponseEntity<?> addBook(@Valid @RequestBody BookDTO bookDto) {
        return ResponseEntity.ok((bookService.addBook(bookDto)));
    }

    @Override
    @PutMapping("/{id}")
    public ResponseEntity<BookDTO> editBook(@PathVariable("id") Long id, @Valid @RequestBody BookDTO updatedBookDto) {
        return ResponseEntity.ok(bookService.editBook(id, updatedBookDto));
    }

    @Override
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteBook(@PathVariable("id") Long id) {
        bookService.deleteBook(id);
        return ResponseEntity.ok().build();
    }
}

