package com.epam.librarycatalogapi.dto;

import com.epam.librarycatalogapi.entity.Genre;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import java.time.LocalDate;

/**
 * @author Arpi Khachatryan on 30.09.2023
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BookDTO {

    private Long id;
    @NotBlank
    private String title;
    @Past
    private LocalDate publicationYear;
    @NotNull
    private AuthorDTO authorDto;
    private Genre genre;
}
