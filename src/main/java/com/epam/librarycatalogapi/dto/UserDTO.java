package com.epam.librarycatalogapi.dto;

import com.epam.librarycatalogapi.entity.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

/**
 * @author Arpi Khachatryan on 30.09.2023
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserDTO {
    private Long id;
    @NotBlank
    private String username;
    @Email
    @NotBlank
    private String email;
}
