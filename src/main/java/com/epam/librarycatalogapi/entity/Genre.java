package com.epam.librarycatalogapi.entity;

/**
 * @author Arpi Khachatryan on 30.09.2023
 */
public enum Genre {

    FICTION,
    MYSTERY,
    NON_FICTION,
    SCIENCE_FICTION,
    ROMANCE
}
