package com.epam.librarycatalogapi.exception;

/**
 * @author Arpi Khachatryan on 30.09.2023
 */
public class AuthorAlreadyExistsException extends BaseException {

    public AuthorAlreadyExistsException(Error error) {
        super(error);
    }
}
