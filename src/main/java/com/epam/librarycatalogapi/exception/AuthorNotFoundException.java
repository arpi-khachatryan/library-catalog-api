package com.epam.librarycatalogapi.exception;

/**
 * @author Arpi Khachatryan on 30.09.2023
 */
public class AuthorNotFoundException extends BaseException {

    public AuthorNotFoundException(Error error) {
        super(error);
    }
}
