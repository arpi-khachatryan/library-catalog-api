package com.epam.librarycatalogapi.exception;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Arpi Khachatryan on 30.09.2023
 */
@Getter
@Setter
public class BaseException extends RuntimeException {

    protected final Error error;

    public BaseException(Error error) {
        this.error = error;
    }
}
