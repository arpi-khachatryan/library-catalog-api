package com.epam.librarycatalogapi.exception;

/**
 * @author Arpi Khachatryan on 30.09.2023
 */
public class BookAlreadyExistsException extends BaseException {

    public BookAlreadyExistsException(Error error) {
        super(error);
    }
}
