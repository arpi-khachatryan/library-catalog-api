package com.epam.librarycatalogapi.exception;

/**
 * @author Arpi Khachatryan on 30.09.2023
 */
public class BookNotFoundException extends BaseException {

    public BookNotFoundException(Error error) {
        super(error);
    }
}
