package com.epam.librarycatalogapi.exception;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;

/**
 * @author Arpi Khachatryan on 30.09.2023
 */
@Getter
@RequiredArgsConstructor
public enum Error {

    USER_NOT_FOUND(404, HttpStatus.NOT_FOUND, "User not found. The requested user does not exist in the system."),

    USER_REGISTRATION_FAILED(400, HttpStatus.BAD_REQUEST, "User registration failed. An account with the provided email already exists."),

    AUTHOR_NOT_FOUND(404, HttpStatus.NOT_FOUND, "Author not found. The requested author does not exist in the system."),

    AUTHOR_ALREADY_EXISTS(400, HttpStatus.BAD_REQUEST, "Author creation failed. An author with the same name already exists in the system."),

    BOOK_NOT_FOUND(404, HttpStatus.NOT_FOUND, "Book not found. The requested book does not exist in the system."),

    BOOK_ALREADY_EXISTS(400, HttpStatus.BAD_REQUEST, "Book creation failed. A book with the same title and author already exists in the system.");

    private final Integer code;
    private final HttpStatus httpStatus;
    private final String message;
}
