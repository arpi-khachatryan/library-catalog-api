package com.epam.librarycatalogapi.exception;

/**
 * @author Arpi Khachatryan on 30.09.2023
 */
public class RegistrationException extends BaseException {

    public RegistrationException(Error error) {
        super(error);
    }
}
