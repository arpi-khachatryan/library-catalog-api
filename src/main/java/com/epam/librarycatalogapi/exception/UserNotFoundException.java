package com.epam.librarycatalogapi.exception;

/**
 * @author Arpi Khachatryan on 30.09.2023
 */
public class UserNotFoundException extends BaseException {

    public UserNotFoundException(Error error) {
        super(error);
    }
}
