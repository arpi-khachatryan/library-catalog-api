package com.epam.librarycatalogapi.mapper;

import com.epam.librarycatalogapi.dto.AuthorDTO;
import com.epam.librarycatalogapi.entity.Author;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface AuthorMapper {

    Author mapToEntity(AuthorDTO authorDto);

    AuthorDTO mapToDto(Author author);

    List<AuthorDTO> mapToDtoList(List<Author> authors);
}