package com.epam.librarycatalogapi.mapper;

import com.epam.librarycatalogapi.dto.BookDTO;
import com.epam.librarycatalogapi.entity.Book;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface BookMapper {

    @Mapping(source = "bookDto.authorDto", target = "author")
    Book mapToEntity(BookDTO bookDto);

    @Mapping(source = "book.author", target = "authorDto")
    BookDTO mapToDto(Book book);

    List<BookDTO> mapToDtoList(List<Book> books);
}