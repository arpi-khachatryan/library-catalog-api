package com.epam.librarycatalogapi.mapper;

import java.util.List;

import com.epam.librarycatalogapi.dto.UserDTO;
import com.epam.librarycatalogapi.entity.User;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UserMapper {

    User mapToEntity(UserDTO userDTO);

    UserDTO mapToDto(User user);

    List<UserDTO> mapToDtoList(List<User> users);
}