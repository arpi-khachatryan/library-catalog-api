package com.epam.librarycatalogapi.repository;

import com.epam.librarycatalogapi.entity.Author;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * @author Arpi Khachatryan on 30.09.2023
 */
public interface AuthorRepository extends JpaRepository<Author, Long>, PagingAndSortingRepository<Author, Long> {

    boolean existsByName(String email);
}
