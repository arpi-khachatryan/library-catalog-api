package com.epam.librarycatalogapi.repository;

import com.epam.librarycatalogapi.entity.Author;
import com.epam.librarycatalogapi.entity.Book;
import com.epam.librarycatalogapi.entity.Genre;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * @author Arpi Khachatryan on 30.09.2023
 */
public interface BookRepository extends JpaRepository<Book, Long>, PagingAndSortingRepository<Book, Long> {

    boolean existsByTitleAndAuthor(String title, Author author);

    Page<Book> findAllByGenre(Genre genre, Pageable pageable);

    Page<Book> findAllByAuthor(Author author, Pageable pageable);
}


