package com.epam.librarycatalogapi.repository;

import com.epam.librarycatalogapi.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * @author Arpi Khachatryan on 30.09.2023
 */
public interface UserRepository extends JpaRepository<User, Long>, PagingAndSortingRepository<User, Long> {

    boolean existsByEmail(String email);
}
