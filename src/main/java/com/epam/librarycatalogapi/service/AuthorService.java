package com.epam.librarycatalogapi.service;

import com.epam.librarycatalogapi.dto.AuthorDTO;

import java.util.List;

/**
 * @author Arpi Khachatryan on 30.09.2023
 */
public interface AuthorService {

    List<AuthorDTO> getAllAuthors(int pageNo, int pageSize, String sortBy, String sortDir);

    AuthorDTO getAuthorById(Long id);

    AuthorDTO createAuthor(AuthorDTO authorDto);

    AuthorDTO updateAuthor(Long id, AuthorDTO updatedAuthorDto);

    void deleteAuthor(Long id);
}
