package com.epam.librarycatalogapi.service;

import com.epam.librarycatalogapi.dto.AuthorDTO;
import com.epam.librarycatalogapi.dto.BookDTO;

import java.util.List;

/**
 * @author Arpi Khachatryan on 30.09.2023
 */
public interface BookService {

    BookDTO addBook(BookDTO bookDto);

    BookDTO editBook(Long id, BookDTO updatedBookDto);

    void deleteBook(Long id);

    BookDTO getBookById(Long id);

    List<BookDTO> getAllBooks(int pageNo, int pageSize, String sortBy, String sortDir);

    List<BookDTO> getBooksByGenre(String genre, int pageNo, int pageSize, String sortBy, String sortDir);

    List<BookDTO> getBooksByAuthor(AuthorDTO authorDto, int pageNo, int pageSize, String sortBy, String sortDir);
}
