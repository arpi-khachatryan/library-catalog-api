package com.epam.librarycatalogapi.service;

import com.epam.librarycatalogapi.dto.UserDTO;

import java.util.List;

/**
 * @author Arpi Khachatryan on 30.09.2023
 */
public interface UserService {

    UserDTO createUser(UserDTO userDto);

    UserDTO updateUser(Long id, UserDTO updatedUserDto);

    void deleteUser(Long id);

    UserDTO getUserById(Long id);

    List<UserDTO> getAllUsers(int pageNo, int pageSize, String sortBy, String sortDir);
}
