package com.epam.librarycatalogapi.service.impl;

import com.epam.librarycatalogapi.dto.AuthorDTO;
import com.epam.librarycatalogapi.entity.Author;
import com.epam.librarycatalogapi.exception.AuthorAlreadyExistsException;
import com.epam.librarycatalogapi.exception.AuthorNotFoundException;
import com.epam.librarycatalogapi.exception.Error;
import com.epam.librarycatalogapi.mapper.AuthorMapper;
import com.epam.librarycatalogapi.repository.AuthorRepository;
import com.epam.librarycatalogapi.service.AuthorService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Arpi Khachatryan on 30.09.2023
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class AuthorServiceImpl implements AuthorService {

    private final AuthorMapper authorMapper;
    private final AuthorRepository authorRepository;

    @Override
    public List<AuthorDTO> getAllAuthors(int pageNo, int pageSize, String sortBy, String sortDir) {
        Sort sort = sortDir.equalsIgnoreCase(Sort.Direction.ASC.name()) ? Sort.by(sortBy).ascending() : Sort.by(sortBy).descending();
        Pageable pageable = PageRequest.of(pageNo, pageSize, sort);
        Page<Author> authors = authorRepository.findAll(pageable);

        if (authors.isEmpty()) {
            log.info("Author not found. The requested author does not exist in the system.");
            throw new AuthorNotFoundException(Error.AUTHOR_NOT_FOUND);
        }
        List<Author> authorList = authors.getContent();
        log.info("Successfully retrieved authors from the database.");
        return new ArrayList<>(authorMapper.mapToDtoList(authorList));
    }

    @Override
    public AuthorDTO getAuthorById(Long id) {
        Author author = authorRepository.findById(id).orElseThrow(() -> new AuthorNotFoundException(Error.AUTHOR_NOT_FOUND));
        log.info("Author successfully found with ID {}: {}", author.getId(), author.getName());
        return authorMapper.mapToDto(author);
    }

    @Override
    public AuthorDTO createAuthor(AuthorDTO authorDto) {
        if (authorRepository.existsByName(authorDto.getName())) {
            log.info("Author with the name '{}' already exists", authorDto.getName());
            throw new AuthorAlreadyExistsException(Error.AUTHOR_ALREADY_EXISTS);
        }
        Author author = authorMapper.mapToEntity(authorDto);
        Author savedAuthor = authorRepository.save(author);
        return authorMapper.mapToDto(savedAuthor);
    }

    @Override
    public AuthorDTO updateAuthor(Long id, AuthorDTO updatedAuthorDto) {
        Author author = authorRepository.findById(id).orElseThrow(() -> new AuthorNotFoundException(Error.AUTHOR_NOT_FOUND));
        log.info("Author with ID '{}' not found", id);
        String name = updatedAuthorDto.getName();
        if (StringUtils.hasText(name)) {
            author.setName(name);
        }
        authorRepository.save(author);
        log.info("The author was successfully stored in the database {}", author.getName());
        return authorMapper.mapToDto(author);
    }

    @Override
    public void deleteAuthor(Long id) {
        if (authorRepository.existsById(id)) {
            authorRepository.deleteById(id);
            log.info("Author has been successfully deleted");
        } else {
            log.info("Author with ID '{}' not found", id);
            throw new AuthorNotFoundException(Error.AUTHOR_NOT_FOUND);
        }
    }
}

