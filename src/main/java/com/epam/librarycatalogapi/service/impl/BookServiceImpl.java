package com.epam.librarycatalogapi.service.impl;

import com.epam.librarycatalogapi.dto.AuthorDTO;
import com.epam.librarycatalogapi.dto.BookDTO;
import com.epam.librarycatalogapi.entity.Author;
import com.epam.librarycatalogapi.entity.Book;
import com.epam.librarycatalogapi.entity.Genre;
import com.epam.librarycatalogapi.exception.BookAlreadyExistsException;
import com.epam.librarycatalogapi.exception.BookNotFoundException;
import com.epam.librarycatalogapi.exception.Error;
import com.epam.librarycatalogapi.mapper.AuthorMapper;
import com.epam.librarycatalogapi.mapper.BookMapper;
import com.epam.librarycatalogapi.repository.BookRepository;
import com.epam.librarycatalogapi.service.BookService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Arpi Khachatryan on 30.09.2023
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class BookServiceImpl implements BookService {

    private final BookMapper bookMapper;
    private final AuthorMapper authorMapper;
    private final BookRepository bookRepository;


    @Override
    public List<BookDTO> getAllBooks(int pageNo, int pageSize, String sortBy, String sortDir) {
        return getBooks(pageNo, pageSize, sortBy, sortDir, null, null);
    }

    @Override
    public List<BookDTO> getBooksByGenre(String genre, int pageNo, int pageSize, String sortBy, String sortDir) {
        return getBooks(pageNo, pageSize, sortBy, sortDir, Genre.valueOf(genre), null);
    }

    @Override
    public List<BookDTO> getBooksByAuthor(AuthorDTO authorDto, int pageNo, int pageSize, String sortBy, String sortDir) {
        Author author = authorMapper.mapToEntity(authorDto);
        return getBooks(pageNo, pageSize, sortBy, sortDir, null, author);
    }


    private List<BookDTO> getBooks(int pageNo, int pageSize, String sortBy, String sortDir, Genre genre, Author author) {
        Sort sort = sortDir.equalsIgnoreCase(Sort.Direction.ASC.name()) ? Sort.by(sortBy).ascending() : Sort.by(sortBy).descending();
        Pageable pageable = PageRequest.of(pageNo, pageSize, sort);

        Page<Book> books = Page.empty();
        if (genre != null) {
            books = bookRepository.findAllByGenre(genre, pageable);
        } else if (author != null) {
            books = bookRepository.findAllByAuthor(author, pageable);
        } else {
            books = bookRepository.findAll(pageable);
        }

        if (books.isEmpty()) {
            log.info("Book not found");
            throw new BookNotFoundException(Error.BOOK_NOT_FOUND);
        }
        List<Book> bookList = books.getContent();
        log.info("Successfully retrieved books from the database.");
        return new ArrayList<>(bookMapper.mapToDtoList(bookList));
    }


    @Override
    public BookDTO getBookById(Long id) {
        Book book = bookRepository.findById(id).orElseThrow(() -> new BookNotFoundException(Error.BOOK_NOT_FOUND));
        log.info("Successfully retrieved the book: {}", book.getTitle());
        return bookMapper.mapToDto(book);
    }

    @Override
    public BookDTO addBook(BookDTO bookDto) {
        Author author = authorMapper.mapToEntity(bookDto.getAuthorDto());
        if (bookRepository.existsByTitleAndAuthor(bookDto.getTitle(), author)) {
            log.info("Book with the same title and author already exists");
            throw new BookAlreadyExistsException(Error.BOOK_ALREADY_EXISTS);
        }
        Book book = bookMapper.mapToEntity(bookDto);
        Book savedBook = bookRepository.save(book);
        return bookMapper.mapToDto(savedBook);
    }

    @Override
    public BookDTO editBook(Long id, BookDTO updatedBookDto) {
        Book book = bookRepository.findById(id).orElseThrow(() -> new BookNotFoundException(Error.BOOK_NOT_FOUND));
        log.info("Book with ID '{}' not found", id);
        LocalDate publicationYear = updatedBookDto.getPublicationYear();
        if (publicationYear != null) {
            book.setPublicationYear(publicationYear);
        }
        bookRepository.save(book);
        log.info("The book was successfully stored in the database {}", book.getTitle());
        return bookMapper.mapToDto(book);
    }

    @Override
    public void deleteBook(Long id) {
        if (bookRepository.existsById(id)) {
            bookRepository.deleteById(id);
            log.info("Book has been successfully deleted");
        } else {
            log.info("Book with ID '{}' not found", id);
            throw new BookNotFoundException(Error.BOOK_NOT_FOUND);
        }
    }
}