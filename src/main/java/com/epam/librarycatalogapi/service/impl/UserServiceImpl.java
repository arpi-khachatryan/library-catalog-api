package com.epam.librarycatalogapi.service.impl;

import com.epam.librarycatalogapi.dto.UserDTO;
import com.epam.librarycatalogapi.entity.User;
import com.epam.librarycatalogapi.exception.Error;
import com.epam.librarycatalogapi.exception.RegistrationException;
import com.epam.librarycatalogapi.exception.UserNotFoundException;
import com.epam.librarycatalogapi.mapper.UserMapper;
import com.epam.librarycatalogapi.repository.UserRepository;
import com.epam.librarycatalogapi.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Arpi Khachatryan on 30.09.2023
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserMapper userMapper;
    private final UserRepository userRepository;

    @Override
    public List<UserDTO> getAllUsers(int pageNo, int pageSize, String sortBy, String sortDir) {
        Sort sort = sortDir.equalsIgnoreCase(Sort.Direction.ASC.name()) ? Sort.by(sortBy).ascending() : Sort.by(sortBy).descending();
        Pageable pageable = PageRequest.of(pageNo, pageSize, sort);
        Page<User> users = userRepository.findAll(pageable);

        if (users.isEmpty()) {
            log.info("No users found matching the criteria.");
            throw new UserNotFoundException(Error.USER_NOT_FOUND);
        }
        List<User> listOfUsers = users.getContent();
        log.info("Successfully retrieved {} users from the database", listOfUsers.size());
        return new ArrayList<>(userMapper.mapToDtoList(listOfUsers));
    }

    @Override
    public UserDTO getUserById(Long id) {
        User user = userRepository.findById(id).orElseThrow(() -> new UserNotFoundException(Error.USER_NOT_FOUND));
        log.info("Successfully retrieved user {} (ID: {}) from the database", user.getUsername(), id);
        return userMapper.mapToDto(user);
    }

    @Override
    public UserDTO createUser(UserDTO userDto) {
        if (userRepository.existsByEmail(userDto.getEmail())) {
            log.info("User registration failed. User with email {} already exists", userDto.getEmail());
            throw new RegistrationException(Error.USER_REGISTRATION_FAILED);
        }
        User user = userMapper.mapToEntity(userDto);
        User save = userRepository.save(user);
        log.info("User {} registered successfully", userDto.getEmail());
        return userMapper.mapToDto(save);
    }

    @Override
    public UserDTO updateUser(Long id, UserDTO updatedUserDto) {
        User user = userRepository.findById(id).orElseThrow(() -> new UserNotFoundException(Error.USER_NOT_FOUND));
        log.info("User with ID {} not found", id);
        String username = updatedUserDto.getUsername();
        String email = updatedUserDto.getEmail();
        if (StringUtils.hasText(username)) {
            user.setUsername(username);
        }
        if (StringUtils.hasText(email)) {
            user.setUsername(email);
        }
        userRepository.save(user);
        log.info("The user {} was successfully updated in the database", user.getUsername());
        return userMapper.mapToDto(user);
    }

    @Override
    public void deleteUser(Long id) {
        if (userRepository.existsById(id)) {
            userRepository.deleteById(id);
            log.info("Successfully deleted the user.");
        } else {
            log.info("User with ID '{}' not found", id);
            throw new UserNotFoundException(Error.USER_NOT_FOUND);
        }
    }
}


