package com.epam.librarycatalogapi.parametrs;

import com.epam.librarycatalogapi.dto.AuthorDTO;
import com.epam.librarycatalogapi.dto.BookDTO;
import com.epam.librarycatalogapi.dto.UserDTO;
import com.epam.librarycatalogapi.entity.Author;
import com.epam.librarycatalogapi.entity.Book;
import com.epam.librarycatalogapi.entity.Genre;
import com.epam.librarycatalogapi.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Arpi Khachatryan on 30.09.2023
 */
public class MockData {

    //user
    public static User getUser() {
        return User.builder()
                .id(1L)
                .username("username")
                .email("email@gmail.com")
                .build();
    }

    public static UserDTO getUserDto() {
        return UserDTO.builder()
                .id(1L)
                .username("username")
                .email("email@gmail.com")
                .build();
    }

    public static List<User> getUserList() {
        List<User> userList = new ArrayList<>();
        userList.add(getUser());
        userList.add(getUser());
        return userList;
    }

    public static List<UserDTO> userListDto() {
        List<UserDTO> userList = new ArrayList<>();
        userList.add(getUserDto());
        userList.add(getUserDto());
        return userList;
    }

    public static Page<User> getUserPage() {
        return new PageImpl<>(List.of(getUser(), getUser()));
    }

    //author
    public static Author getAuthor() {
        return Author.builder()
                .id(1L)
                .name("name")
                .build();
    }

    public static AuthorDTO getAuthorDto() {
        return AuthorDTO.builder()
                .id(1L)
                .name("name")
                .build();
    }

    public static Page<Author> getAuthorPage() {
        return new PageImpl<>(List.of(getAuthor(), getAuthor()));
    }

    public static List<Author> getAuthorList() {
        List<Author> authorList = new ArrayList<>();
        authorList.add(getAuthor());
        authorList.add(getAuthor());
        return authorList;
    }

    public static List<AuthorDTO> authorListDto() {
        List<AuthorDTO> authorList = new ArrayList<>();
        authorList.add(getAuthorDto());
        authorList.add(getAuthorDto());
        return authorList;
    }

    //book
    static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
    static LocalDate publicationDate = LocalDate.parse("29.09.2023", formatter);

    public static Book getBook() {
        return Book.builder()
                .id(1L)
                .author(getAuthor())
                .genre(Genre.MYSTERY)
                .publicationYear(publicationDate)
                .title("title")
                .build();
    }

    public static BookDTO getBookDto() {
        return BookDTO.builder()
                .id(1L)
                .authorDto(getAuthorDto())
                .genre(Genre.MYSTERY)
                .publicationYear(publicationDate)
                .title("title")
                .build();
    }

    public static Page<Book> getBookPage() {
        return new PageImpl<>(List.of(getBook(), getBook()));
    }

    public static List<Book> getBookList() {
        List<Book> bookList = new ArrayList<>();
        bookList.add(getBook());
        bookList.add(getBook());
        return bookList;
    }

    public static List<BookDTO> bookListDto() {
        List<BookDTO> bookList = new ArrayList<>();
        bookList.add(getBookDto());
        bookList.add(getBookDto());
        return bookList;
    }
}