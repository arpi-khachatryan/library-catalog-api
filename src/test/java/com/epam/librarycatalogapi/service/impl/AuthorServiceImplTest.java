package com.epam.librarycatalogapi.service.impl;

import com.epam.librarycatalogapi.dto.AuthorDTO;
import com.epam.librarycatalogapi.mapper.AuthorMapper;
import com.epam.librarycatalogapi.repository.AuthorRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.List;
import java.util.Optional;

import static com.epam.librarycatalogapi.parametrs.MockData.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @author Arpi Khachatryan on 30.09.2023
 */
@ExtendWith(MockitoExtension.class)
public class AuthorServiceImplTest {

    @InjectMocks
    private AuthorServiceImpl authorService;

    @Mock
    private AuthorMapper authorMapper;

    @Mock
    private AuthorRepository authorRepository;

    @Test
    public void testGetAllAuthors() {
        //given
        var pageNo = 0;
        var pageSize = 10;
        var sortBy = "name";
        var sortDir = "asc";
        var authorList = getAuthorList();
        var authorPage = getAuthorPage();
        var authorListDto = authorListDto();

        //when
        when(authorRepository.findAll(any(Pageable.class))).thenReturn(authorPage);
        when(authorMapper.mapToDtoList(anyList())).thenReturn(authorListDto);
        List<AuthorDTO> result = authorService.getAllAuthors(pageNo, pageSize, sortBy, sortDir);

        //then
        assertNotNull(result);
        assertEquals(2, result.size());
        verify(authorRepository).findAll(PageRequest.of(pageNo, pageSize, Sort.by(sortBy).ascending()));
        verify(authorMapper).mapToDtoList(authorList);
    }


    @Test
    public void testCreateAuthor() {
        //given
        var authorDto = getAuthorDto();
        var author = getAuthor();

        //when
        when(authorRepository.existsByName(authorDto.getName())).thenReturn(false);
        when(authorMapper.mapToEntity(authorDto)).thenReturn(author);
        when(authorRepository.save(author)).thenReturn(author);
        when(authorMapper.mapToDto(author)).thenReturn(authorDto);
        AuthorDTO result = authorService.createAuthor(authorDto);

        //then
        assertNotNull(result);
    }

    @Test
    public void testUpdateAuthor() {
        //given
        var authorId = 1L;
        var updatedAuthorDto = getAuthorDto();
        var author = getAuthor();

        //when
        when(authorRepository.findById(authorId)).thenReturn(Optional.of(author));
        when(authorRepository.save(author)).thenReturn(author);
        when(authorMapper.mapToDto(author)).thenReturn(updatedAuthorDto);
        AuthorDTO result = authorService.updateAuthor(authorId, updatedAuthorDto);

        //then
        assertNotNull(result);
        assertEquals("name", result.getName());
    }

    @Test
    public void testDeleteAuthor() {
        //given
        var authorId = 1L;

        //when
        when(authorRepository.existsById(authorId)).thenReturn(true);

        //then
        assertDoesNotThrow(() -> authorService.deleteAuthor(authorId));
    }
}
