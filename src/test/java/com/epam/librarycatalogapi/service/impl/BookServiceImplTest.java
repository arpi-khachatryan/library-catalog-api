package com.epam.librarycatalogapi.service.impl;


import com.epam.librarycatalogapi.dto.AuthorDTO;
import com.epam.librarycatalogapi.dto.BookDTO;
import com.epam.librarycatalogapi.entity.Author;
import com.epam.librarycatalogapi.mapper.AuthorMapper;
import com.epam.librarycatalogapi.mapper.BookMapper;
import com.epam.librarycatalogapi.repository.BookRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import static com.epam.librarycatalogapi.parametrs.MockData.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @author Arpi Khachatryan on 30.09.2023
 */
@ExtendWith(MockitoExtension.class)
public class BookServiceImplTest {


    @InjectMocks
    private BookServiceImpl bookService;

    @Mock
    private BookMapper bookMapper;


    @Mock
    private BookRepository bookRepository;

    @Mock
    private AuthorMapper authorMapper;

    @Test
    public void testGetAllBooks() {
        //given
        var pageNo = 0;
        var pageSize = 10;
        var sortBy = "name";
        var sortDir = "ASC";
        var bookList = getBookList();
        var bookPage = getBookPage();
        var bookListDto = bookListDto();

        //when
        when(bookRepository.findAll(any(Pageable.class))).thenReturn(bookPage);
        when(bookMapper.mapToDtoList(anyList())).thenReturn(bookListDto);
        List<BookDTO> result = bookService.getAllBooks(pageNo, pageSize, sortBy, sortDir);

        //then
        assertNotNull(result);
        assertEquals(2, result.size());
        verify(bookRepository).findAll(PageRequest.of(pageNo, pageSize, Sort.by(sortBy).ascending()));
        verify(bookMapper).mapToDtoList(bookList);
    }

    @Test
    public void testGetBookById() {
        //given
        var bookId = 1L;
        var book = getBook();

        //when
        when(bookRepository.findById(bookId)).thenReturn(Optional.of(book));
        when(bookMapper.mapToDto(book)).thenReturn(new BookDTO());

        //when
        BookDTO result = bookService.getBookById(bookId);

        //then
        assertNotNull(result);
    }

    @Test
    public void testAddBook() {
        //when
        var bookDto = getBookDto();
        var book = getBook();
        var author = getAuthor();
        var authorDTO = getAuthorDto();

        //when
        when(authorMapper.mapToEntity(any(AuthorDTO.class))).thenReturn(author);
        when(bookRepository.existsByTitleAndAuthor(anyString(), any(Author.class))).thenReturn(false);
        when(bookMapper.mapToEntity(bookDto)).thenReturn(book);
        when(bookRepository.save(book)).thenReturn(book);
        when(bookMapper.mapToDto(book)).thenReturn(bookDto);
        BookDTO result = bookService.addBook(bookDto);

        //then
        assertNotNull(result);
    }

    @Test
    public void testEditBook() {
        //given
        var bookId = 1L;
        var updatedBookDto = getBookDto();
        var book = getBook();
        var expectedDate = LocalDate.of(2023, 9, 29);

        //when
        when(bookRepository.findById(bookId)).thenReturn(Optional.of(book));
        when(bookRepository.save(book)).thenReturn(book);
        when(bookMapper.mapToDto(book)).thenReturn(updatedBookDto);
        BookDTO result = bookService.editBook(bookId, updatedBookDto);

        //then
        assertNotNull(result);
        assertEquals(expectedDate, result.getPublicationYear());
    }

    @Test
    public void testDeleteBook() {
        //given
        var bookId = 1L;

        //when
        when(bookRepository.existsById(bookId)).thenReturn(true);

        //then
        assertDoesNotThrow(() -> bookService.deleteBook(bookId));
    }
}
