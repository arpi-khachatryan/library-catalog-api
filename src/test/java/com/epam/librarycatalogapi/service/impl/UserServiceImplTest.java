package com.epam.librarycatalogapi.service.impl;

import com.epam.librarycatalogapi.dto.UserDTO;
import com.epam.librarycatalogapi.mapper.UserMapper;
import com.epam.librarycatalogapi.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.List;
import java.util.Optional;

import static com.epam.librarycatalogapi.parametrs.MockData.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

/**
 * @author Arpi Khachatryan on 30.09.2023
 */
@ExtendWith(MockitoExtension.class)
public class UserServiceImplTest {

    @InjectMocks
    private UserServiceImpl userService;

    @Mock
    private UserMapper userMapper;

    @Mock
    private UserRepository userRepository;

    @Test
    public void testGetAllUsers() {
        //given
        var pageNo = 0;
        var pageSize = 10;
        var sortBy = "name";
        var sortDir = "ASC";
        var userList = getUserList();
        var userPage = getUserPage();
        var userListDto = userListDto();

        //when
        when(userRepository.findAll(any(Pageable.class))).thenReturn(userPage);
        when(userMapper.mapToDtoList(anyList())).thenReturn(userListDto);
        List<UserDTO> result = userService.getAllUsers(pageNo, pageSize, sortBy, sortDir);

        //then
        assertNotNull(result);
        assertEquals(2, result.size());
        verify(userRepository).findAll(PageRequest.of(pageNo, pageSize, Sort.by(sortBy).ascending()));
        verify(userMapper).mapToDtoList(userList);
    }

    @Test
    public void testGetUserById() {
        //given
        var userId = 1L;
        var user = getUser();

        //when
        when(userRepository.findById(userId)).thenReturn(Optional.of(user));
        when(userMapper.mapToDto(user)).thenReturn(new UserDTO());
        UserDTO result = userService.getUserById(userId);

        //then
        assertNotNull(result);
    }

    @Test
    public void testCreateUser() {
        //when
        var userDto = getUserDto();
        var user = getUser();

        //when
        when(userRepository.existsByEmail(anyString())).thenReturn(false);
        when(userMapper.mapToEntity(userDto)).thenReturn(user);
        when(userRepository.save(user)).thenReturn(user);
        when(userMapper.mapToDto(user)).thenReturn(userDto);
        UserDTO result = userService.createUser(userDto);

        //then
        assertNotNull(result);
    }

    @Test
    public void testUpdateUser() {
        //when
        var userId = 1L;
        var updatedUserDto = getUserDto();
        var user = getUser();

        //when

        when(userRepository.findById(userId)).thenReturn(Optional.of(user));
        when(userRepository.save(user)).thenReturn(user);
        when(userMapper.mapToDto(user)).thenReturn(updatedUserDto);
        UserDTO result = userService.updateUser(userId, updatedUserDto);

        //then
        assertNotNull(result);
        assertEquals("username", result.getUsername());
    }

    @Test
    public void testDeleteUser() {
        //when
        var userId = 1L;

        //when
        when(userRepository.existsById(userId)).thenReturn(true);

        //then
        assertDoesNotThrow(() -> userService.deleteUser(userId));
    }
}

